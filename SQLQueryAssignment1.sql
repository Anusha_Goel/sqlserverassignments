create database Assignment1

use Assignment1

create table Worker ( WORKER_ID int primary key,
FIRST_NAME CHAR(25) not null,
LAST_NAME CHAR(25),
SALARY int check(SALARY between 10000 and 25000),
JOINING_DATE DATETIME,
DEPARTMENT CHAR(25) check(DEPARTMENT in ('HR','Accts','Sales','IT')))

insert into Worker values
(001,'Monika','Arora',11000,'2014-02-20 00:00:00','HR'),
(002,'Niharika','Verma',20000,'2017-06-30 00:00:00','Sales'),
(003,'Vishul','Singhal',15000,'2017-07-05 00:00:00','Accts'),
(004,'Vivek','Raj',11000,'2022-03-30 00:00:00','IT'),
(005,'Niharika','Singh',11000,'2019-06-23 00:00:00','IT'),
(006,'Suresh','Verma',11000,'2023-11-11 00:00:00','HR'),
(007,'Geetiks','Singhal',17000,'2017-06-30 00:00:00','Sales')

--Q1
Select FIRST_NAME AS WORKER_NAME FROM Worker

Select UPPER(FIRST_NAME) AS upper_workerName from Worker

Select DISTINCT DEPARTMENT AS Distinct_Dept FROM Worker

Select LEFT(FIRST_NAME,3) AS WORKER_NAME FROM Worker

Select FIRST_NAME ,CHARINDEX('a',FIRST_NAME) AS WORKER_NAME from Worker

select RTRIM(FIRST_NAME) from Worker
select LTRIM(DEPARTMENT) AS AM from Worker

select DISTINCT(DEPARTMENT), LEN(DEPARTMENT) from Worker

select FIRST_NAME,REPLACE(FIRST_NAME,'a','A') from Worker

SELECT FIRST_NAME + LAST_NAME AS COMPLETE_NAME FROM Worker

select CONCAT(TRIM(FIRST_NAME),'   ', TRIM(LAST_NAME)) AS COMPLETE_NAME from Worker

select * from Worker ORDER BY FIRST_NAME

select * from Worker ORDER BY FIRST_NAME ASC,DEPARTMENT DESC

select * from Worker where FIRST_NAME IN ( 'Monika','Suresh')

select * from Worker where FIRST_NAME NOT IN ( 'Monika','Suresh')

select * from Worker where DEPARTMENT='Sales'
select * from Worker where DEPARTMENT='Accts'

select * from Worker where FIRST_NAME LIKE '%g%'
select * from Worker where FIRST_NAME LIKE '%a'
select * from Worker where FIRST_NAME LIKE '%l'

select * from Worker where FIRST_NAME LIKE '____k'

select * from worker where SALARY BETWEEN 10000 AND 15000

select count(DEPARTMENT) from Worker where DEPARTMENT='HR'

SELECT * FROM Worker

select * from Worker where month(JOINING_DATE)=06 AND year(JOINING_DATE)=2017 